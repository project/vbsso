<?php
/**
 * -----------------------------------------------------------------------
 * vBSSO is a solution which helps you connect to different software platforms
 * via secure Single Sign-On.
 *
 * Copyright (c) 2011-2017 vBSSO. All Rights Reserved.
 * This software is the proprietary information of vBSSO.
 *
 * Author URI: http://www.vbsso.com
 * License: GPL version 2 or later -
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------
 */

namespace Drupal\vbsso\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Path\CurrentPathStack;

/**
 * Class VbssoMainController.
 *
 * @package Drupal\vbsso\Controller
 */
class VbssoMainController extends ControllerBase {

    const VERSION = '1.1.1';
    protected $currentUser;

    /**
     * VbssoMainController constructor.
     *
     * @param AccountProxy $current_user current user
     * @param CurrentPathStack $currentpath path
     *
     * @return mixed
     */
    public function __construct(AccountProxy $current_user, CurrentPathStack $currentpath) {
        $this->currentUser = $current_user;
        $this->currentPath = $currentpath;
    }

    /**
     * Create container
     *
     * @param ContainerInterface $container container
     *
     * @return static
     *
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('current_user'),
            $container->get('path.current')
        );
    }

    /**
     * Single entry point to slave.
     *
     * @return void
     */
    public function init() {
        $options = $this->config('config.' . VBSSO_PRODUCT_ID);
        sharedapi_data_handler(
            SHAREDAPI_PLATFORM_DRUPAL, \Drupal::VERSION, VbssoMainController::VERSION,
            $options->get(VBSSO_NAMED_EVENT_FIELD_API_KEY),
            [
                SHAREDAPI_EVENT_VERIFY => 'vbsso_listener_verify',
                SHAREDAPI_EVENT_LOGIN => 'vbsso_listener_register',
                SHAREDAPI_EVENT_AUTHENTICATION => 'vbsso_listener_authentication',
                SHAREDAPI_EVENT_LOGOUT => 'vbsso_listener_logout',
                SHAREDAPI_EVENT_REGISTER => 'vbsso_listener_register',
                SHAREDAPI_EVENT_CREDENTIALS => 'vbsso_listener_credentials',
            ]
        );
    }

    /**
     * Override system route for registration new user.
     *
     * @return \Drupal\Core\Routing\TrustedRedirectResponse
     *   Returning front page.
     */
    public function registerRoute() {
        $url = sharedapi_url_add_destination(variable_get(VBSSO_NAMED_EVENT_FIELD_REGISTER_URL, NULL), TRUE, '', variable_get(VBSSO_NAMED_EVENT_FIELD_LID, NULL));

        if (empty($url)) {
            $url = $this->getFrontPage();
        }

        return new TrustedRedirectResponse($url);
    }

    /**
     * Override system route for lost password.
     *
     * @return \Drupal\Core\Routing\TrustedRedirectResponse
     *   Returning front page.
     */
    public function lostPasswordRoute() {
        $url = sharedapi_url_add_destination(variable_get(VBSSO_NAMED_EVENT_FIELD_LOSTPASSWORD_URL, NULL), TRUE, '', variable_get(VBSSO_NAMED_EVENT_FIELD_LID, NULL));

        if (empty($url)) {
            $url = $this->getFrontPage();
        }

        return new TrustedRedirectResponse($url);
    }

    /**
     * Override system route for logout.
     *
     * @return void
     */
    public function logoutRoute() {

        $url = sharedapi_url_add_destination(variable_get(VBSSO_NAMED_EVENT_FIELD_LOGOUT_URL, NULL), TRUE, '', variable_get(VBSSO_NAMED_EVENT_FIELD_LID, NULL));

        if (preg_match('/(\/user\/\d)/i', urldecode($url))) {
            $newUrl = new Url('<front>', [], ['absolute' => TRUE]);
            $url = sharedapi_url_add_destination(variable_get(VBSSO_NAMED_EVENT_FIELD_LOGOUT_URL, NULL), FALSE, $newUrl->toString(), variable_get(VBSSO_NAMED_EVENT_FIELD_LID, NULL));
        }

        if (empty($url)) {
            $url = $this->getFrontPage();
        }

        $redirect = new TrustedRedirectResponse($url);
        $redirect->send();
    }

    /**
     * Override system route for view profile.
     *
     * @return \Drupal\Core\Routing\TrustedRedirectResponse
     *   Returning front page
     */
    public function viewProfileRoute() {
        $user = $this->currentUser->getEmail();
        $url = variable_get(VBSSO_NAMED_EVENT_FIELD_PROFILE_URL, '') . md5($user);

        if (empty($url)) {
            $url = $this->getFrontPage();
        }

        return new TrustedRedirectResponse($url);
    }

    /**
     * Override system route for view/edit profile.
     *
     * @return void
     */
    public function editProfileRoute() {
        $url = variable_get(VBSSO_NAMED_EVENT_FIELD_PROFILE_URL, '');
        $currentUser = $this->currentUser->id();
        $path = $this->currentPath->getpathInfo();
        $args = explode('/', $path);
        $user = User::load($args[2]);

        if ($currentUser !== $user->id()) {
            $url .= md5(strtolower($user->getEmail()));
        }

        if (empty($url)) {
            $url = $this->getFrontPage();
        }

        $redirect = new TrustedRedirectResponse($url);
        $redirect->send();
    }

    /**
     * Get front page url.
     *
     * @return string
     *   Returning front page
     */
    protected function getFrontPage() {
        $front = new Url('<front>', [], ['absolute' => TRUE]);
        return $front->toString();
    }

}
