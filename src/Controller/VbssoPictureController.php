<?php
/**
 * -----------------------------------------------------------------------
 * vBSSO is a solution which helps you connect to different software platforms
 * via secure Single Sign-On.
 *
 * Copyright (c) 2011-2017 vBSSO. All Rights Reserved.
 * This software is the proprietary information of vBSSO.
 *
 * Author URI: http://www.vbsso.com
 * License: GPL version 2 or later -
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------
 */

namespace Drupal\vbsso\Controller;

use Drupal\user\Entity\User;
use Drupal\file\Entity\File;

/**
 * Class VbssoPictureController.
 *
 * @package Drupal\vbsso\Controller
 */
class VbssoPictureController {

    /**
     * Temporary path.
     *
     * @var string
     */
    protected $path;

    /**
     * Url to user avatar from master.
     *
     * @var string
     */
    protected $url;

    /**
     * Cache image folder.
     */
    const IMAGE_CACHE_FOLDER = 'public://styles/thumbnail/public/vbsso_pictures/';

    /**
     * VbssoPictureController constructor.
     *
     * @param string $path Path to save user pictures.
     * @param string $url Image url from master.
     * 
     * @return mixed
     */
    public function __construct($path, $url) {
        $this->path = $path;
        $this->url = $url;

        $this->deleteImageCache();
    }

    /**
     * Get user avatar from vBulletin.
     *
     * @param \Drupal\user\Entity\User $user Current logged user.
     * 
     * @return mixed
     */
    public function initialize(User $user) {

        $this->savePicture();

        $file = File::create(
            [
                'uid' => 1,
                'filename' => $this->getFileName(),
                'uri' => $this->path . $this->getFileName(),
                'status' => 1,
            ]
        );
        $file->save();

        $user->set('user_picture', $file);
        $user->save();
    }

    /**
     * Save picture from vBulletin.
     *
     * @return \Drupal\file\FileInterface|false
     *   Returning file_save_date.
     */
    public function savePicture() {

        $baa_username = sharedapi_decode_data(
            variable_get(VBSSO_NAMED_EVENT_FIELD_API_KEY, SHAREDAPI_DEFAULT_API_KEY), variable_get(VBSSO_NAMED_EVENT_FIELD_BAA_USERNAME, NULL)
        );

        $baa_password = sharedapi_decode_data(
            variable_get(VBSSO_NAMED_EVENT_FIELD_API_KEY, SHAREDAPI_DEFAULT_API_KEY), variable_get(VBSSO_NAMED_EVENT_FIELD_BAA_PASSWORD, NULL)
        );

        $options = [
            "ssl" => [
                "verify_peer" => FALSE,
                "verify_peer_name" => FALSE,
            ],
        ];

        if ($baa_username) {
            $options['http'] = [
                'header' => "Authorization: Basic " . base64_encode("$baa_username:$baa_password"),
            ];
        }

        $context = stream_context_create($options);
        $picture = file_get_contents($this->url, FALSE, $context);
        file_prepare_directory($this->path, FILE_CREATE_DIRECTORY);
        return file_save_data($picture, $this->path . $this->getFileName(), FILE_EXISTS_REPLACE);
    }

    /**
     * Create temporary file name.
     *
     * @return mixed
     *   Return url with hash.
     */
    protected function getFileName() {
        return md5($this->url) . $this->getExtension();
    }

    /**
     * Get image extension.
     *
     * @return string
     *   Return Image extension.
     */
    protected function getExtension() {
        $imageSize = getimagesize($this->url);

        if (!$imageSize) {
            return '.jpg';
        }

        $ext = explode('/', $imageSize['mime']);
        $ext = '.' . $ext[1];
        return $ext;
    }

    /**
     * Delete user image cache.
     * 
     * @return void
     */
    protected function deleteImageCache() {
        $image = self::IMAGE_CACHE_FOLDER . $this->getFileName();
        if (file_exists($image)) {
            unlink($image);
        }
    }

}
