<?php
/**
 * -----------------------------------------------------------------------
 * vBSSO is a solution which helps you connect to different software platforms
 * via secure Single Sign-On.
 *
 * Copyright (c) 2011-2017 vBSSO. All Rights Reserved.
 * This software is the proprietary information of vBSSO.
 *
 * Author URI: http://www.vbsso.com
 * License: GPL version 2 or later -
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * -----------------------------------------------------------------------
 *
 */

require_once(dirname(__FILE__) . '/functions.php');

drupal_add_js(strip_tags(VBSSO_PLATFORM_FOOTER_GA_HTML(sharedapi_get_platforms(SHAREDAPI_PLATFORM_DRUPAL))), array('type' => 'inline', 'scope' => 'footer'));

if (variable_get(VBSSO_PLATFORM_FOOTER_LINK_PROPERTY, VBSSO_PLATFORM_FOOTER_LINK_SHOW_EVERYWHERE)) {
    drupal_add_js("document.write('" . VBSSO_PLATFORM_FOOTER_LINK_HTML . "');", array('type' => 'inline', 'scope' => 'footer'));
}


/**
 * Implements hook_help().
 */
function vbsso7_help($path, $arg) {
    return vbsso_help_hook($path, $arg);
}

/**
 * Implements hook_menu().
 */
function vbsso7_menu() {
    return vbsso_menu_hook();
}

function vbsso7_menu_alter(&$items) {
    vbsso_menu_alter_hook($items);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function vbsso7_form_user_login_block_alter(&$form, &$form_state) {
    vbsso_form_user_login_block_alter_hook($form, $form_state);
}

/**
 * Form builder; Configure the vbsso system.
 *
 * @ingroup forms
 */
function vbsso7_admin_form() {
    $form = vbsso_admin_form_hook();

    $form[VBSSO_PRODUCT_ID]['actions'] = array('#type' => 'actions');
    $form[VBSSO_PRODUCT_ID]['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save Changes'),
    );

    return $form;
}

function vbsso7_admin_form_submit($form, &$form_state) {
    vbsso_admin_form_submit_hook($form, $form_state);
}

/**
 * Implements hook_user_view() (drupal 7.x).
 */
function vbsso7_user_view($account) {
    global $user;

    if (variable_get(VBSSO_NAMED_EVENT_FIELD_PROFILE_URL, '') != '') {
        $profile_link = '';
        if ($account->uid != $user->uid) {
            if (variable_get(VBSSO_NAMED_EVENT_FIELD_SHOW_VBULLETIN_AUTHOR_PROFILE, 1)) {
                $profile_link = variable_get(VBSSO_NAMED_EVENT_FIELD_PROFILE_URL, '') . md5(strtolower($account->mail));
            }
        } else {
            if (variable_get(VBSSO_NAMED_EVENT_FIELD_SHOW_VBULLETIN_PROFILE, 1) && !vbsso_is_user_admin()) {
                $profile_link = variable_get(VBSSO_NAMED_EVENT_FIELD_PROFILE_URL, '');
            }
        }
        sharedapi_url_redirect($profile_link);
    }
}

/**
 * Implements hook_preprocess_user_picture().
 */
function vbsso7_preprocess_user_picture(&$variables) {
    if (variable_get(VBSSO_NAMED_EVENT_FIELD_FETCH_AVATARS, 1) AND variable_get(VBSSO_NAMED_EVENT_FIELD_AVATAR_URL, '')) {
        // Load the full user object since it is not provided with nodes, comments, or views displays.
        $account = user_load($variables['account']->uid);
        $picture = variable_get(VBSSO_NAMED_EVENT_FIELD_AVATAR_URL, '') . md5(strtolower($account->mail)) . '&thumb=true';

        $variables['user_picture'] = '';

        if (!empty($picture)) {
            $alt = t("@user's picture", array('@user' => format_username($account)));
            if (module_exists('image') && file_valid_uri($picture) && $style = variable_get('user_picture_style', '')) {
                $variables['user_picture'] = theme('image_style', array('style_name' => $style, 'path' => $picture, 'alt' => $alt, 'title' => $alt));
            } else {
                $variables['user_picture'] = theme('image', array('path' => $picture, 'alt' => $alt, 'title' => $alt));
            }

            if ($account->uid && user_access('access user profiles')) {
                // Create link to the user's profile.
                $attributes = array('title' => t('View user profile.'));
                $variables['user_picture'] = l($variables['user_picture'], 'user/' . $account->uid, array('attributes' => $attributes, 'html' => TRUE));
            } elseif (!empty($account->homepage)) {
                // If user is anonymous, create link to the commenter's homepage.
                $attributes = array(
                    'title' => t('View user website.'),
                    'rel' => 'external nofollow',
                );
                $variables['user_picture'] = l($variables['user_picture'], $account->homepage, array('attributes' => $attributes, 'html' => TRUE));
            }
        }
    }
}

//if (variable_get(VBSSO_PLATFORM_FOOTER_LINK_PROPERTY, VBSSO_PLATFORM_FOOTER_LINK_SHOW_EVERYWHERE)) {
//    function vbsso7_block_info() {
//        $blocks['vbsso_footer'] = array(
//            'info' => t('vBSSO Footer'),
//            'region' => 'footer',
//            'status' => 1,
//        );
//        return $blocks;
//    }
//
//    function vbsso7_block_view($delta = '') {
//      switch ($delta) {
//          case 'vbsso_footer':
//              $block['content'] = VBSSO_PLATFORM_FOOTER_LINK_HTML;
//              break;
//      }
//
//      return $block;
//    }
//}

function vbsso7_form_user_profile_form_alter(&$form, &$form_state) {
    vbsso_listener_link_edit_profile($form);
}

function vbsso7_form_user_login_alter(&$form, &$form_state) {
    vbsso_form_user_login_block_alter_hook($form, $form_state, TRUE);
}
